<?php

return [
    'hive_icon' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:hive_barometer/Resources/Public/Icons/hive_32x32.svg'
    ],
];
