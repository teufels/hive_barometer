// Progress bars
$(document).ready(function() {

    if ( $(".tx-hive-barometer").length ) {
        $(".barometer-item").each(function( index ) {
            var min = $(this).attr("aria-valuemin");
            var max = $(this).attr("aria-valuemax");
            var val = $(this).attr("aria-valuenow");
            var percent = Math.round( ((val - min) * 100) / (max - min) );

            // Default animation
            if ($(".barometer-item").data("animation") === "default") {
                $(this).find(".progress .progress-bar").animate({ width:percent + "%" },{duration: 3000, queue: false});
                $(this).find(".progress-number.valuenow").animate({ left:percent + "%" },{duration: 3000, queue: false});
            }

            // No Animation
            if ($(".barometer-item").data("animation") === "no") {
                $(this).find(".progress .progress-bar").css('width', percent + "%");
                $(this).find(".progress-number.valuenow").css('left', percent + "%");
            }

            //Custom Animation
            //set in own template EXT
        });
    }

});
