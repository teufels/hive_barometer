<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'HiveBarometer',
        'Barometerlist',
        [
            \HIVE\HiveBarometer\Controller\BarometerController::class => 'list'
        ],
        // non-cacheable actions
        [
            \HIVE\HiveBarometer\Controller\BarometerController::class => 'list'
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_barometer/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig">'
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_barometer/Configuration/TsConfig/Page/BackendPreview.tsconfig">'
    );
    // Add backend preview hook
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['hive_barometer']
        = HIVE\HiveBarometer\Hooks\PageLayoutViewHook::class;
})();



