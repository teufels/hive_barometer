<?php

declare(strict_types=1);

namespace HIVE\HiveBarometer\Domain\Repository;

use HIVE\HiveBarometer\Domain\Model\Barometer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * This file is part of the "HIVE>Barometer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 teufels GmbH <digital@teufels.com>
 */

/**
 * The repository for Barometer
 */
class BarometerRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param String $uids
     * @return array|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findByUids(String $uids)
    {
        $idList = GeneralUtility::intExplode(',',$uids, true);
        $list = [];
        foreach ($idList as $id) {
            $item = $this->findByIdentifier($id);
            if ($item) {
                $list[] = $item;
            }
        }
        return $list;
    }
}
