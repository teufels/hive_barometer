<?php

declare(strict_types=1);

namespace HIVE\HiveBarometer\Controller;


/**
 * This file is part of the "HIVE>Barometer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 teufels GmbH <digital@teufels.com>
 */

/**
 * BarometerController
 */
class BarometerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * barometerRepository
     *
     * @var \HIVE\HiveBarometer\Domain\Repository\BarometerRepository
     */
    protected $barometerRepository = null;

    /**
     * @param \HIVE\HiveBarometer\Domain\Repository\BarometerRepository $barometerRepository
     */
    public function injectBarometerRepository(\HIVE\HiveBarometer\Domain\Repository\BarometerRepository $barometerRepository)
    {
        $this->barometerRepository = $barometerRepository;
    }

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $singleRecords = (string)$this->settings['singleRecords'];
        $barometers = $this->barometerRepository->findByUids($singleRecords);
        $this->view->assign('barometers', $barometers);
        return $this->htmlResponse();
    }


}
