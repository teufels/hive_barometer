![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__barometer-blue.svg)
![version](https://img.shields.io/badge/version-1.0.*-yellow.svg?style=flat-square)

HIVE Barometer
==========
HIVE Barometer

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_barometer`

## Documentation
- Create "Barometer" in Backend 
- place Plugin and select "Barometer" to display
- select "Animation" 
    - On "Custom" you could create own Animation in your Theme EXT

## Requirements
- `jQuery`
- `"typo3/cms-core": "^11.5"`