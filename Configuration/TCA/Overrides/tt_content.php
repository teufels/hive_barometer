<?php
defined('TYPO3') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HiveBarometer',
    'Barometerlist',
    'HIVE>Barometer',
    'EXT:hive_barometer/Resources/Public/Icons/hive_32x32.svg',
    'HIVE'
);

$pluginSignature = 'hivebarometer_barometerlist';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$pluginSignature] = 'hive_icon';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:hive_barometer/Configuration/FlexForms/flexform_list.xml'
);