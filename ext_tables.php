<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivebarometer_domain_model_barometer', 'EXT:hive_barometer/Resources/Private/Language/locallang_csh_tx_hivebarometer_domain_model_barometer.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivebarometer_domain_model_barometer');
})();
