<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'HIVE>Barometer',
    'description' => 'HIVE Barometer',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.3',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
